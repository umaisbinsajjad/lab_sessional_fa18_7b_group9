package la1;

import java.util.Scanner;

public class simpleWithdraw implements Bank {
	double balance;
	
	simpleWithdraw(){
		balance=50000.95;
	}
	
	@Override
	   public void withdraw() {
		Scanner sc= new Scanner(System.in);
		
	     System.out.println("Enter amount to withdraw: ");
	     double amount=sc.nextDouble();
	     
	     
	     if (amount>500 && amount<=balance) {
	    	 balance=balance-amount;
	    	 System.out.println("Rs. "+ amount +" withdrawn successfully. Remaining balance Rs:"+ balance);
	     }
	     
	     else {
	    	 System.out.println("Amount is too less or exceeded your balance.");
	     }
	     
	     
	   }
}
