package la1;

import java.util.Scanner;

public class unlimitCredit implements Bank {
	double balance;
	
	unlimitCredit(){
		balance=50000.99;
	}
	
	@Override
	public void withdraw() {
		Scanner sc= new Scanner(System.in);
		
	     System.out.println("Enter amount to withdraw: ");
	     double amount=sc.nextDouble();
	     
	     
	     if (amount>500) {
	    	 balance=balance-amount;
	    	 System.out.println("Rs. "+ amount +" withdrawn successfully.");
	     }
	     
	     else {
	    	 System.out.println("Amount must be greater than 500.");
	     }
	   }
}