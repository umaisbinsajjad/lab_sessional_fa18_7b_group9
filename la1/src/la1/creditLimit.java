package la1;

import java.util.Scanner;

public class creditLimit implements Bank {
	double balance;
	double limit;
	
	creditLimit(){
		balance=50000.99;
		limit=10000;
	}
	
	@Override
	   public void withdraw() {

	Scanner sc= new Scanner(System.in);
	
    System.out.println("Enter amount to withdraw: ");
    double amount=sc.nextDouble();
    
    
    if (amount>500 && amount<=limit) {
   	 balance=balance-amount;
   	 System.out.println("Rs. "+ amount +" withdrawn successfully. Remaining balance Rs:"+ balance);
    }
    
    else {
   	 System.out.println("Amount must not exceeded the limit.");
    }
}
}