package la1;

public class CheckType {
	public static Bank getType(String withdrawtype){
	      if(withdrawtype == null){
	         return null;
	      }		
	      if(withdrawtype.equalsIgnoreCase("SIMPLE")){
	         return new simpleWithdraw();
	         
	      } else if(withdrawtype.equalsIgnoreCase("LIMIT")){
	         return new creditLimit();
	         
	      } else if(withdrawtype.equalsIgnoreCase("UNLIMITED")){
	         return new unlimitCredit();
	      }
	      
	      return null;
	   }
}
