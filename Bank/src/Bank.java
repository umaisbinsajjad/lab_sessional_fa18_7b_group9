
public interface Bank {
	void deposit();
	void withdraw();
	void transfer();
}
