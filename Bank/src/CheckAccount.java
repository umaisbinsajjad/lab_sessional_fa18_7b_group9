public class CheckAccount {
	 //use getShape method to get object of type shape 
	   public static Bank getAccount(String accountType){
	      if(accountType == null){
	         return null;
	      }		
	      if(accountType.equalsIgnoreCase("BASIC")){
	         return new basic();
	         
	      } else if(accountType.equalsIgnoreCase("GOLD")){
	         return new gold();
	         
	      } else if(accountType.equalsIgnoreCase("PREMIUM")){
	         return new premium();
	      }
	      
	      return null;
	   }
}
